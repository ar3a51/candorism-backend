import * as express from 'express';
import * as winston from 'winston';

export const asyncErrHandler = (handler:any) => {
    return async (req:express.Request, res:express.Response,next:express.NextFunction) => {
        try {
            await handler(req,res);
        } catch (ex) {
            console.log("an error thrown");
            next(ex);
        }
    }
}

