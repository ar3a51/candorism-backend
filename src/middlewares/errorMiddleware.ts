import * as express from 'express';
import { Error } from 'mongoose';
import * as winston from 'winston';

export class ErrorMiddleware {
    constructor(){

    }

    public errorMiddleWare(err:Error, req:express.Request, res:express.Response, next:express.NextFunction): void {
        winston.error({
            name: err.name,
            message: err.message,
            stack: err.stack,
        })
        res.sendStatus(500);
    }
}