export class SocketIOStartup{

        private _socket: any;

       constructor(
        private _io: any,
       ){}

       public startSocket():void {
         this._io.on("connection", (socket:any)=> {
            this._socket = socket;

            socket.on("message", this.messageEvent)

         })
       }

       private messageEvent(message: any):void {
            
       }
}