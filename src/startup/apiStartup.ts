import * as express from 'express';
import * as mongoSanitize from 'express-mongo-sanitize';
import { authenticateMiddleware } from "../middlewares/authenticateMiddleware";
import * as cors from 'cors';

import {PostServiceCrud}       from '../services/postServices/postCRUD';
import {PostQuery}       from '../services/postServices/postQuery';

import {ReplyServiceCRUD}        from '../services/replyServices/replyServiceCRUD';
import {ReplyServiceQuery}       from '../services/replyServices/replyServiceQuery';

import {UserServiceCRUD}                        from '../services/userServices/userServiceCRUD';
import {UserServiceQuery}                        from '../services/userServices/userServiceQuery';

import { SignInService }                                      from '../services/administrationService/signInService';

import { MessageCrud }                       from '../services/messageServices/messageCrud';
import { MessageQuery }                       from '../services/messageServices/messageQuery';
import { ImageQuery }                      from '../services/imageServices/imageQuery';

import {UserRoute}  from '../routes/user/userRoutes';
import {PostRoute} from '../routes/post/postRoutes';
import {ReplyRoute} from '../routes/post/replyRoutes';
import {LoginRoutes} from '../routes/administration/loginRoutes';
import {MessageRoutes}   from '../routes/message/messageRoutes';
import { ImageRoute }           from '../routes/image/imageRoute';

import {ErrorMiddleware}           from '../middlewares/errorMiddleware';

export class ApiStartup {

    private _postRoute:PostRoute;
    private _loginRoute: LoginRoutes;
    private _userRoute: UserRoute;
    private _replyRoute: ReplyRoute;
    private _messageRoute: MessageRoutes;
    private _imageRoute: ImageRoute;
  

    constructor(
        private _app: express.Express,
        private _route: express.Router,
        private _errMiddleware: ErrorMiddleware
    ){
        _app.use(express.json({limit: '50mb'}));
        _app.use(mongoSanitize());
        this.initRoutes(_route);
    }

    private initRoutes(router: express.Router):void {
        let postServiceCrud:PostServiceCrud = new PostServiceCrud();
        let postQuery:PostQuery = new PostQuery();
        let signInService:SignInService = new SignInService();
        let userServiceQuery:UserServiceQuery = new UserServiceQuery();
        let userCrudService: UserServiceCRUD = new UserServiceCRUD();
        let replyCrud: ReplyServiceCRUD = new ReplyServiceCRUD();
        let replyQuery: ReplyServiceQuery = new ReplyServiceQuery();
        let messageCrud: MessageCrud = new MessageCrud();
        let messageQuery: MessageQuery = new MessageQuery();
        let imageQuery: ImageQuery = new ImageQuery();

        this._postRoute = new PostRoute(postServiceCrud,postQuery,express.Router());
        this._loginRoute = new LoginRoutes(signInService,express.Router());
        this._userRoute = new UserRoute(userServiceQuery, userCrudService,express.Router());
        this._replyRoute = new ReplyRoute(replyCrud,replyQuery,express.Router());
        this._messageRoute = new MessageRoutes(messageCrud, messageQuery,express.Router());
        this._imageRoute = new ImageRoute(imageQuery, express.Router());

    }

    public start():void {
    
        this._app.use(cors());
        this._app.get("/", (req, res)=>{
           res.send('System is working');
        });

        this._app.use("/image", this._imageRoute.getRoute());
        this._app.use("/signin",this._loginRoute.getRoute());
    

        this._app.use(authenticateMiddleware);
        this._app.use("/user",this._userRoute.getRoute());
        this._app.use("/post", this._postRoute.getRoute());
        this._app.use("/reply",this._replyRoute.getRoute());
        this._app.use("/message", this._messageRoute.getRoute());

        this._app.use(this._errMiddleware.errorMiddleWare);
    }


}