import { UserModel, IUser } from '../../models/users';
import { UserDetailsModel } from '../../models/userDetails';
import * as bcryptjs from 'bcryptjs';
import { UserServiceQuery }   from './userServiceQuery';

import {IUserDetails}   from '../../models/userDetails';

export class UserServiceCRUD {
    constructor(){}

    public async registerUser(user) {

        let userModel:IUser = new UserModel({
            username: user.username,
            password: await bcryptjs.hash(user.password,process.env.BCRYPT_SEED_HASH),
        });

       let userModelResult:IUser = await userModel.save();

        let userDetailsModel:IUserDetails = new UserDetailsModel({
            username: userModelResult.username,
            firstname: user.firstname,
            middlename: user.middlename,
            lastname: user.lastname,
            photoprofile: user.photoprofile,
            address1: user.address1,
            address2: user.address2,
            suburb: user.suburb,
            postcode: user.postcode,
        });

        let userDetailsModelResult:IUserDetails = await userDetailsModel.save();

      

       return userModelResult;
    }

    async updatePassword(user){

        try {
            let userQuery = new UserServiceQuery();
            let userObj = await userQuery.getUserByUsername(user.username);
            
            userObj.set({password: user.password});
            await user.save();
        } catch(ex) {
            throw ex;
        }

    }

}