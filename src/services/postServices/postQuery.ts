import { PostModel, IPost }                   from '../../models/posts';
import { LikeModel, ILike }                   from '../../models/likes';

import { UserServiceQuery }       from '../userServices/userServiceQuery';
import { PostSearchApiService }                        from '../searchApiServices/postSearchServices';


interface IResult {
    _id: string,
    username: string,
    photoProfile: Buffer,
    message: string,
    postType: string,
    dateTime: Date,
    likes: any,
    image: string,
    description: string,
    location: {
        latitude: string,
        longitude: string,
    }
}

export class PostQuery {

    private userQuery:UserServiceQuery = null;
    private _postSearch:PostSearchApiService = null;

    constructor() {
        this.userQuery = new UserServiceQuery();
        this._postSearch = new PostSearchApiService();
    }

    public async getPost(pageNumber: number, username: string) {
        
        let limit = 5;
        let results = [];
        let posts = await PostModel.find({username}).sort({dateTime: 'desc'}).limit(limit).skip((pageNumber - 1) * limit);
        
        let result = await Promise.all(posts.map(async (p)=> {
            return await this.mapToResult(p);
        }));
       
       return result;

    }

    public async getPostById(postId:string) {
        try {
            let post = await PostModel.findById(postId);
            return await this.mapToResult(post);
        } catch(ex){
            throw ex;
        }
    }

    public async searchPost(searchTerm:string){
        let searchResult = await this._postSearch.searchIndex(searchTerm);
        let postIds:string[] = searchResult.map(v => {return v.id});

        let posts = await PostModel.find({_id: {$in: postIds}});
        
        let result = await Promise.all(posts.map(async (p)=> {
            return await this.mapToResult(p);
        }));
        

       return result;

    }

    private async getLikers(postId: string): Promise<any[]> {
        let likes = await LikeModel.find({postId})
      
        return await Promise.all(likes.map(async like => {
            let user = await this.userQuery.getUserDetailsByUsername(like.username);

            return {
               photoProfile: user.photoProfile,
               username: like.username,
            }
        }))

        
    } 

    private async mapToResult(post:IPost):Promise<IResult> {

        let user = await this.userQuery.getUserDetailsByUsername(post.username);
        let likes = await this.getLikers(post.id);

        let result:IResult = {
            _id: post.id,
            username : post.username,
            photoProfile: user.photoProfile,
            message: post.message,
            dateTime: post.dateTime,
            postType: post.postType,
            location: {
                latitude: post.location.latitude,
                longitude: post.location.longitude
            },
            likes: {
                count: likes.length,
                likers: likes
            },
            description: (post.image && post.image.imageFile && post.image.imageFile.length > 0) ? post.image.description : "",
            image: (post.image && post.image.imageFile && post.image.imageFile.length > 0) ? post.image.imageFile.toString("base64") : ""
        }

        return result;
    }
}