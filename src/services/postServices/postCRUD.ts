import { 
        PostModel, 
        IPost, 
        IPostDomainModel 
    }       from '../../models/posts';
import { LikeModel }       from '../../models/likes';
import * as postApi   from '../searchApiServices/postSearchServices';
import axios from "axios";


export class PostServiceCrud {

    private _postSearchService: postApi.PostSearchApiService;

    constructor(){
        this._postSearchService = new postApi.PostSearchApiService();
    }

    async insertPost(post:IPostDomainModel)
    {
        let image: { imageFile: Buffer, description: string, extension:string} = null;
        let cognitiveApi: string = process.env.COGNITIVE_API;
        let tags:string[] = [];
        let imageDescription: string = "";
        let postedImage:any;

        if (post.image && post.image.imageFile && post.image.imageFile.length > 0)
            image = {
                imageFile: post.image.imageFile, 
                description: "",
                extension: post.image.extension
            };
        
        try {
            let postModel = new PostModel({
                username: post.username,
                message: post.message,
                image,
                postType: post.postType,
                location: post.location,
            });

            let postResult:IPost = await postModel.save();
            
            if (postResult.image && postResult.image.imageFile && postResult.image.imageFile.length > 0) {
                postedImage = await axios.get(`${cognitiveApi}/api/${postResult.id}`).then(response => {return response.data;});
                imageDescription = postedImage.image.description;
                tags = postedImage.image.tags;
            }
            
          this._postSearchService.InsertIntoIndex({
                id: postResult.id,
                message: postResult.message,
                tags: tags,
                imageDescription: imageDescription,
                username: postResult.username
            }).then(resp => {console.log("post indexed")})
            .catch((err:any) =>{console.log(err.response.data)});

            return {
                _id: postResult.id,
                message: postResult.message,
                username: postResult.username,
                postedDate: postResult.dateTime,
                postType: postResult.postType,
                image: {
                    description: (postedImage)?postedImage.image.description:"",
                    extension: (postResult.image)?postResult.image.extension:"",
                    imageFile: (postResult.image)?postResult.image.imageFile:"",
                },
            };
            
        } catch (ex){
            throw ex;
        }

    }

    async updatePost(post: {id: any, message: string}) {
        let currPost = await PostModel.findById(post.id);
        currPost.set({message: post.message});
        
        return await currPost.save();
    }

    async like(postId: string, username: string){
        console.log(postId);
        console.log(username);
        let like = new LikeModel({
            postId,
            username
        });
       
        return await like.save();
    }
}