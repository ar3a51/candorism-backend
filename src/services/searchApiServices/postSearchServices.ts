import axios from "axios";

export interface IPostIndex {
    id:string;
    message: string,
    tags: string[],
    imageDescription: string,
    username: string,
}

export class PostSearchApiService {
    private _searchApi:string;

    constructor(){
        this._searchApi = process.env.SEARCH_API_ENDPOINT;
    }

    public async InsertIntoIndex (postIndex: IPostIndex):Promise<any> {
        return await axios.post(`${this._searchApi}`,{
            value: [{
                "@search.action":"upload",
                id: postIndex.id,
                message: postIndex.message,
                tags: postIndex.tags,
                imageDescription: postIndex.imageDescription,
                username: postIndex.username
            }]
        } ,{
            headers: {'api-key': process.env.SEARCH_API_ADMIN_KEY }
        })
    }

    public async searchIndex(searchTerm: string):Promise<any>{
        return await axios.get(`${process.env.SEARCH_API_ENDPOINT_POST_INDEX_SEARCH}${searchTerm}`,{
            headers: {
                "api-key": process.env.SEARCH_API_ADMIN_KEY
            }
        })
        .then( (resp) => {
             return resp.data["value"]; 
        })
    }
}