import {UserServiceQuery}   from '../userServices/userServiceQuery';
import {IUser}              from '../../models/users';
import { IUserDetails } from '../../models/userDetails';
import * as bcryptjs from 'bcryptjs';
import * as jsonwebtoken from 'jsonwebtoken';


export class SignInService {
    private _userService:UserServiceQuery;

    constructor(){
        this._userService = new UserServiceQuery();
    }

    public async verifyUser(username: string, password: string):Promise<any> {
    
        let authenticationResult: {
            result: boolean,
            userObject: any
        }
        let user:IUser = await this._userService.getUserByUsername(username);
        
        if (user){
            let isAuthenticated = await bcryptjs.compare(password,user.password);
            
            if (isAuthenticated){
                let userDetails:IUserDetails = await this._userService.getUserDetailsByUsername(username);
                let objAuthentication = {
                    username: user.username,
                    name: `${userDetails.firstname} ${userDetails.lastname}`,
                    suburb: userDetails.suburb 
                }
                authenticationResult = { result: true, userObject : objAuthentication }
                
               return Promise.resolve(authenticationResult);

            }
        }
        authenticationResult = { result: false, userObject : null }

        return Promise.resolve(authenticationResult);
        
    }

    public generateWebToken(userObj: any):string{
        return jsonwebtoken.sign(userObj,process.env.SECRET);
    }
}