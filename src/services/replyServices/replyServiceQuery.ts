import { ReplyModel }       from '../../models/replies';
import { UserServiceQuery }                   from '../userServices/userServiceQuery';
import * as mongoose from 'mongoose';


export class ReplyServiceQuery {

    _userQuery = null;
    constructor() {
        this._userQuery = new UserServiceQuery();
    }
    
    async GetReply(replyId){
        
        let reply = await ReplyModel.findById(replyId);
        let user = await this._userQuery.getUserDetailsByUsername(reply.username);

        return this.MapToReturnObject(reply, user);
    }

    async GetReplyByPostId(postId: string, pageNumber: number, username:string) {
        let limit = 5;
        let response = [];

        let user = await this._userQuery.getUserDetailsByUsername(username);
        let replies = await ReplyModel.find({
            "post_id": mongoose.Types.ObjectId(postId),
            "parent_reply_id": null  
        }).limit(limit).skip((pageNumber - 1) * limit);

        response = await Promise.all(replies.map(r => {return this.MapToReturnObject(r, user)}));
        return response;
    }

    async GetChildReplies(parentCommentId: string, pageNumber: number, username: string){

        let limit = 5;
        let response = [];

        let user = await this._userQuery.getUserDetailsByUsername(username);

        let comments = await ReplyModel.find({
            "parent_reply_id": mongoose.Types.ObjectId(parentCommentId),
        }).limit(limit).skip((pageNumber - 1) * limit);

        response = await Promise.all(comments.map(c => {return this.MapToReturnObject(c, user)}));        
        return response;
    }

    MapToReturnObject(reply, user){

        return {
            id: reply.id,
            message: reply.message,
            user: {
                username: user.username,
                photo: user.photoprofile,
            },
            date: reply.date,
            modifiedDate: reply.modifiedDate,
        }
    }
}