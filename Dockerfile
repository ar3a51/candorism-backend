FROM node:10
WORKDIR /candorism-backend
COPY package.json /candorism-backend
RUN npm install
COPY . /candorism-backend
CMD node index.js
EXPOSE 3030